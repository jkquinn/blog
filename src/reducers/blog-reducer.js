import { actionTypes } from '../blogActionTypes';

const initialState = {
    blogs: [],
    comments: [],
    loading: false,
    editing: false,
    editBlogId: 0,
    showComment: false,
    commentBlogId: 0,
}

export const blogReducer = (state = initialState, action) => {

    switch (action.type) {
        case actionTypes.ADD_BLOG_REQUEST:
            return { ...state, loading: true };

        case actionTypes.ADD_BLOG_DONE:
            return { 
                ...state, 
                loading: false, 
                blogs: state.blogs.concat(action.blog) 
            };

        case actionTypes.EDIT_BLOG:
            return { ...state, editing: true, editBlogId: action.editBlogId };

        case actionTypes.EDIT_SAVE_REQUEST:
            return { ...state, loading: true };

        case actionTypes.EDIT_SAVE_DONE:
            return { ...state, loading: false, editing: false, editBlogId: 0 };

        case actionTypes.REFRESH_BLOGS_REQUEST:
            return { ...state, loading: true };

        case actionTypes.REFRESH_BLOGS_DONE:
            return { ...state, loading: false, blogs: action.blogs };

        case actionTypes.DELETE_BLOG_REQUEST:
            return { ...state, loading: false };

        case actionTypes.DELETE_BLOG_DONE:
            return { ...state, loading: false, blogs: state.blogs.filter(o => o.id !== action.blogId) };

        
        case actionTypes.ADD_COMMENT_REQUEST:
            return { ...state, loading: true };

        case actionTypes.ADD_COMMENT_DONE:
        return { 
            ...state, 
            loading: false,
            showComment: false,
            comments: state.comments.concat(action.comment) 
        };
        
        case actionTypes.CANCEL_BLOG:
            return { ...state, editing: false, editBlogId: 0 };

        case actionTypes.ADD_COMMENT_BUTTON:
            return { ...state, showComment: true, commentBlogId: action.commentBlogId};
        case actionTypes.REFRESH_COMMENTS_REQUEST:
            return { ...state, loading: true };
            case actionTypes.REFRESH_COMMENTS_DONE:
            return { ...state, loading: false, comments: action.comments };
        default:
            return state;
    }

}
