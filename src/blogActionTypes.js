import keyMirror from 'key-mirror';

export const actionTypes = keyMirror({
    ADD_BLOG_REQUEST: null,
    ADD_BLOG_DONE: null,
    EDIT_BLOG: null,
    EDIT_SAVE_REQUEST: null,
    EDIT_SAVE_DONE: null,
    REFRESH_BLOGS_REQUEST: null,
    REFRESH_BLOGS_DONE: null,
    DELETE_BLOG_REQUEST: null,
    DELETE_BLOG_DONE: null,

    ADD_COMMENT_REQUEST: null,
    ADD_COMMENT_DONE: null,
    CANCEL_BLOG: null,
    ADD_COMMENT_BUTTON: null,
    REFRESH_COMMENTS_REQUEST: null,
    REFRESH_COMMENTS_DONE: null,

});