import { actionTypes } from '../blogActionTypes';

export const refreshCommentsRequestActionCreator = () => ({ type: actionTypes.REFRESH_COMMENTS_REQUEST});
export const refreshCommentsDoneRequestActionCreator = comments => ({ type: actionTypes.REFRESH_COMMENTS_DONE, comments});

export const refreshComments = comments => {
    return dispatch => {
        dispatch(refreshCommentsRequestActionCreator());
        return fetch('http://localhost:3050/comments')
        .then(res => res.json())
        .then(comments => dispatch(refreshCommentsDoneRequestActionCreator(comments)));
    };
};