import { actionTypes } from '../blogActionTypes';

export const editBlogActionCreator = (id) =>
    ({ type: actionTypes.EDIT_BLOG, editBlogId: id });

export const cancelBlogActionCreator = () =>
    ({ type: actionTypes.CANCEL_BLOG});

export const saveBlogRequestActionCreator = () => ({ type: actionTypes.EDIT_SAVE_REQUEST });
export const saveBlogDoneRequestActionCreator = blog => ({ type: actionTypes.EDIT_SAVE_DONE, blog });

export const saveBlog = blog => {
    return dispatch => {
        dispatch(saveBlogRequestActionCreator());
        return fetch('http://localhost:3050/blogs/' + encodeURIComponent(blog.id), {
            method: 'PUT',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(blog),
        })
            .then(res => res.json())
            .then(blog => dispatch(saveBlogDoneRequestActionCreator(blog)));
    };
};