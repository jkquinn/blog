import { actionTypes } from '../blogActionTypes';

export const refreshBlogsRequestActionCreator = () => ({ type: actionTypes.REFRESH_BLOGS_REQUEST});
export const refreshBlogsDoneRequestActionCreator = blogs => ({ type: actionTypes.REFRESH_BLOGS_DONE, blogs});

export const refreshBlogs = blogs => {
    return dispatch => {
        dispatch(refreshBlogsRequestActionCreator());
        return fetch('http://localhost:3050/blogs')
        .then(res => res.json())
        .then(blogs => dispatch(refreshBlogsDoneRequestActionCreator(blogs)));
    };
};