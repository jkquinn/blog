import { actionTypes } from '../blogActionTypes';

export const addCommentButtonActionCreator = (commentBlogId) =>
    ({ type: actionTypes.ADD_COMMENT_BUTTON, commentBlogId });

export const addCommentRequestActionCreator = () => ({ type: actionTypes.ADD_COMMENT_REQUEST });
export const addCommentDoneRequestActionCreator = (comment) => ({ type: actionTypes.ADD_COMMENT_DONE, comment });

export const addComment = (comment) => {

    return dispatch => {
        dispatch(addCommentRequestActionCreator());
        return fetch('http://localhost:3050/comments', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(comment),
        })
            .then(res => res.json())
            .then(comment => dispatch(addCommentDoneRequestActionCreator(comment)));
    };
};

