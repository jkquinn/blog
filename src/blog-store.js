import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { blogReducer } from './reducers/blog-reducer';
import { composeWithDevTools } from 'redux-devtools-extension';

export const appStore = createStore(
    blogReducer,
    composeWithDevTools(applyMiddleware(thunk))
  );
  