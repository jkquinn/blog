import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { Provider } from 'react-redux';
import { appStore } from './blog-store';
import { BlogToolContainer } from './components/BlogToolContainer'

ReactDOM.render(<Provider store={appStore}>
    <BlogToolContainer />
    </Provider>, document.getElementById('root'));
