import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { addBlog } from '../actions/add-blog';
import { addComment } from '../actions/add-comment';
import { removeBlog } from '../actions/delete-blog';
import { saveBlog } from '../actions/save-blog';
import { refreshBlogs } from '../actions/refresh-blogs';
import { refreshComments } from '../actions/refresh-comments';
import { editBlogActionCreator } from '../actions/save-blog';
import { cancelBlogActionCreator } from '../actions/save-blog';
import { addCommentButtonActionCreator } from '../actions/add-comment';

import { BlogTool } from './BlogTool';

export const BlogToolContainer = connect(
  // mapStateToProps - data from the state and maps it to props passed into the component
  ({ blogs, comments, editing, editBlogId, showComment, commentBlogId }) => ({ blogs, comments, editing, editBlogId, showComment, commentBlogId }),
  // mapDispatchToProps - bind action creators to the store's dispatch function and passed them in as props
  // to the component
  dispatch => bindActionCreators({
    refreshBlogs, add: addBlog, remove: removeBlog, save: saveBlog, edit: editBlogActionCreator,
    addComment, addButton: addCommentButtonActionCreator, refreshComments,
    cancel: cancelBlogActionCreator
  }, dispatch),
)(BlogTool);
