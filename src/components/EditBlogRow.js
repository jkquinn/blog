import * as React from 'react';

export class EditBlogRow extends React.Component {

    saveBlog = () => {
        this.props.save({
            id: this.props.blog.id,
            title: this.titleInput.value,
            body: this.bodyInput.value,
            date: new Date().toLocaleDateString(),
        })
    }

    compoentnDidMount() {
        if (this.titleInput) {
            this.titleInput.focus();
        }
    }

    render() {
        return <tr>
            <td><input type="text" name="title"
                ref={input => this.titleInput = input}
                defaultValue={this.props.blog.title} /></td>
            <td><textarea name="body"
                ref={input => this.bodyInput = input}
                defaultValue={this.props.blog.body} ></textarea></td>
            <td>{this.props.blog.date}</td>
            <td></td>
            <td>
                <button className="posButton" onClick={this.saveBlog}>Save</button>
            </td>
            <td>
                <button className="negButton" onClick={this.props.cancel}>Cancel</button>
            </td>
        </tr>;
    }


}