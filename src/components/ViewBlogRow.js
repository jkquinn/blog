import * as React from 'react';

export const ViewBlogRow = (props) => {

    return <tr>
        <td>{props.blog.title}</td>
        <td>{props.blog.body}</td>
        <td>{props.blog.date}</td>
        <td>
            {props.comments.map(c => (props.blog.id === c.blogId)
                ? <p>{c.comment} - {c.name}</p>
            : <p></p>)}
        </td>
        <td><button className="posButton" onClick={() => props.edit(props.blog.id)}>Edit</button></td>
        <td><button className="negButton" onClick={() => props.remove(props.blog.id)}>Delete</button></td>
        <td><button className="posButton" onClick={() => props.addButton(props.blog.id)}>Add</button></td>
    </tr>;
}