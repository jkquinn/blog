import * as React from 'react';

export class BlogForm extends React.Component {

    constructor(props) {
        super(props);
    
        this.state = {
          title: '',
          body: '',
        };
      }
    
      onChange = e => {
        this.setState({
          [ e.target.name ]: e.target.value
        });
      }

      render() {

        return <form className="addBlogForm">
          <div>
            <label>Title:</label>
            <input type="text" name="title"
              value={this.state.title} onChange={this.onChange} />
          </div>
          <div>
            <label>Body:</label>
            <input type="text" name="body"
              value={this.state.body} onChange={this.onChange} />
          </div>
          <button className="posButton" type="button" onClick={() => this.props.onSubmitBlog({
            ...this.state
          })}>Submit</button>
        </form>
    
      }
        
        
}