import * as React from 'react';

export class CommentForm extends React.Component {

    constructor(props) {
        super(props);
    
        this.state = {
          name: '',
          comment: '',
          blogId: 0,
        };
      }

      saveComment = () => {
        this.props.onSubmitComment({
            name: this.state.name,
            comment: this.state.comment,
            blogId: this.props.blogId,
        })
    }
    
      onChange = e => {
        this.setState({
          [ e.target.name ]: e.target.value
        });
      }

      render() {

        return <form className="addBlogForm">
          <div>
            <label>Name:</label>
            <input type="text" name="name"
              value={this.state.name} onChange={this.onChange} />
          </div>
          <div>
            <label>Comment:</label>
            <input type="text" name="comment"
              value={this.state.comment} onChange={this.onChange} />
          </div>
          <button className="posButton" type="button" onClick={this.saveComment}>Submit</button>
        </form>
    
      }
        
        
}