import * as React from 'react';

import { BlogForm } from './BlogForm';
import { BlogTable } from './BlogTable';
import { CommentForm } from './CommentForm';

export class BlogTool extends React.Component {

    componentDidMount() {
        this.props.refreshBlogs()
        .then(() => this.props.refreshComments());
    }


    render() {
        return <React.Fragment>
            {!this.props.showComment
            ? <div><BlogTable blogs={this.props.blogs}
                remove={this.props.remove}
                edit={(id) => this.props.edit(id)}
                editBlogId={this.props.editBlogId}
                save={blog => this.props.save(blog).then(() => this.props.refreshBlogs())}
                cancel={this.props.cancel}
                addButton={(id) => this.props.addButton(id)}
                comments={this.props.comments}
            /> <BlogForm onSubmitBlog={blog =>
                this.props.add(blog).then(() =>
                    this.props.refreshBlogs())}
            /></div>
            : <CommentForm onSubmitComment={(comment) =>
                this.props.addComment(comment).then(() =>
                    this.props.refreshBlogs())} blogId={this.props.commentBlogId}
            />}

            

            

        </React.Fragment>;
    };
};