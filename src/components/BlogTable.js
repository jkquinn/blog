import * as React from 'react';

import { ViewBlogRow } from './ViewBlogRow';
import { EditBlogRow } from './EditBlogRow';

export const BlogTable = (props) => <table width="100%">
    <thead>
        <tr>
            <th>Title</th>
            <th>Body</th>
            <th>Date</th>
            <th>Comments</th>
            <th>Edit</th>
            <th>Delete</th>
            <th>Add Comment</th>
        </tr>
    </thead>
    <tbody>
        {props.blogs.map(blog => (props.editBlogId === blog.id)
            ? <EditBlogRow blog={blog} remove={props.remove} save={props.save} cancel={props.cancel}/>
            : <ViewBlogRow blog={blog} remove={props.remove} edit={props.edit} addButton={props.addButton} commentBlogId={props.commentBlogId} comments={props.comments} />)}

    </tbody>


</table>